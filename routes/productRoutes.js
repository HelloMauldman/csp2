const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers")
const auth = require("../auth")


// retrieve all products
router.get("/all", (req, res)=>{
	productController.getAllProducts().then(result => res.send(result))
});

//retrieve all ACTIVE products
router.get("/active", (req, res)=>{
	productController.getAllActiveProduct().then(result=>res.send(result))
});

//create product admin only
router.post("/", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		userAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body
	}
	productController.createProduct(data).then(result =>res.send(result))
});

//getspecific product

router.get("/:productId", (req, res)=>{
	productController.getProduct(req.params.productId).then(result=>res.send(result))
})


// //update product admin only
router.put("/:productId", auth.verify,(req, res)=>{
	let data = {
		tokenId: auth.decode(req.headers.authorization),
		productId:req.params.productId,
		reqBody: req.body
	}
	productController.updateProduct(data).then(result=>res.send(result))
});

router.put("/:productId/archive", auth.verify, (req, res)=>{
	let data = {
		tokenId: auth.decode(req.headers.authorization),
		productId:req.params.productId
	}
	if(data.tokenId.isAdmin == true){
		productController.archiveProduct(data).then(result => res.send(result))
	}
	else{
		res.send("User is not an admin.")
	}
})

router.put("/:productId/activate", auth.verify, (req, res)=>{
	let data = {
		tokenId: auth.decode(req.headers.authorization),
		productId:req.params.productId
	}
	if(data.tokenId.isAdmin == true){
		productController.activateProduct(data).then(result => res.send(result))
	}
	else{
		res.send("User is not an admin.")
	}
})


module.exports = router