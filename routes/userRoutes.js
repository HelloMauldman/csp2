const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers")
const auth = require("../auth.js");

//register
router.post("/register", (req, res)=>{
	userController.register(req.body).then(result => res.send(result))
});

//User Authenticate login probs
router.post("/login", (req, res)=>{
	userController.login(req.body).then(result=>res.send(result))
})

// //set user as Admin
router.put("/:id/set-admin", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin == true){
		userController.updateAccess(req.params.id).then(result => res.send(result))
	}
	else{
		res.send("User is not an admin.")
	}
});


// //optional thing to do or stretch goals
//get all users
router.get("/", auth.verify, (req, res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	if(isAdmin == true){
		userController.getAllUsers().then(result => res.send(result))
	}
	else{
		res.send("User is not an admin.")
	}
});

//get single user
// router.get("/:id", (req, res)=>{
// 	userController.getUser(req.params.id).then(result => res.send(result))
// });

router.delete("/delete-user/:id", auth.verify, (req,res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	if(isAdmin == true){
		userController.deleteUser(req.params.id).then(result => res.send(result))
	}
	else{
		res.send("User is not an admin.")
	}
})


// orders

router.get("/my-order", auth.verify, (req, res)=>{
	let data = auth.decode(req.headers.authorization)
	userController.getMyOrders(data).then(result=>res.send(result))
})

router.get("/orders", auth.verify, (req, res)=>{
	let data = auth.decode(req.headers.authorization).isAdmin
	if(data == true){
		userController.getAllOrders(data).then(result=>res.send(result))
	}
	else{
		res.send("User is not Admin.")
	}
})


//requires isAdmin
router.post("/checkout", auth.verify, (req, res)=>{
	let data = {
		tokenId: auth.decode(req.headers.authorization),
		cart: req.body
	}
	userController.checkout(data).then(result=>res.send(result))
})



module.exports = router;