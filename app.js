const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");



const app = express();
let PORT = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


mongoose.connect("mongodb+srv://admin:admin1234@zuitt-bootcamp.exnj7.mongodb.net/capstoneProj2?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});





app.use("/users", userRoutes);
app.use("/products", productRoutes);

const db = mongoose.connection;
db.on("error", console.error.bind(console,"Connection Error:"));
db.once("open", ()=>{console.log("Houston, we are connected!")});


app.listen(process.env.PORT || 3001, () => console.log (`Server is running at ${PORT}`));

