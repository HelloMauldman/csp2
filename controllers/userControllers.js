const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/product")

module.exports.register = (reqBody) => {
	// let newUser = new User({
	// 	email: reqBody.email,
	// 	password:bcrypt.hashSync(reqBody.password, 10)
	// })

	// return newUser.save().then((result, error)=>{
	// 	if(error){
	// 		return false;
	// 	}
	// 	else{
	// 		// console.log(result)
	// 		return result
	// 	}
	// })
	return User.findOne({email:reqBody.email}).then((result, error)=>{
		if(result !== null && result.email == reqBody.email){
			return "Email already exists."
		}
		else{
			let newUser = new User({
				email: reqBody.email,
				password:bcrypt.hashSync(reqBody.password, 10)
			})
			return newUser.save().then((result, error)=>{
				if(error){
					return false;
			}
			else{
				return result;
			}
		})
	}
	})

}

module.exports.login = (reqBody)=>{
	return User.findOne({email: reqBody.email}).then((result, error)=>{
		if(result == null){
			return "email is null.";
		}
		else{
			let isPswdCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPswdCorrect == true){
				return {access: auth.createAccessToken(result)}
			}
		}
	})
}

module.exports.updateAccess = (userId, userBody) =>{
	return User.findByIdAndUpdate(userId, {isAdmin:true}, {returnDocument:"after"}).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result
		}
	})
}

module.exports.getAllUsers = () =>{
	return User.find().then((result, error)=>{
		if(error){
			return false
		}
		else{
			return result
		}
	})
}

module.exports.getUser = (userId)=>{
	return User.findById(userId).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}


module.exports.deleteUser = (userId) =>{
	return User.findByIdAndDelete(userId).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}

module.exports.getMyOrders = (data)=>{
	return User.findById({_id:data.id}).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result.orders;
		}
	})
}


module.exports.getAllOrders = (data)=>{
	return User.find().then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}


module.exports.checkout = async (data) =>{
	const {tokenId, cart} = data;
//REAL CODE
	return User.findById(tokenId.id).then(user =>{
		if(user == null){
			return false
		}
		else{
			user.orders.push({
				products:cart.products,
				totalAmount:cart.totalAmount
			})

			return user.save().then((updatedUser, error)=>{
				if(error){
					return false;
				}
				else{
					const currentOrder = updatedUser.orders[updatedUser.orders.length-1]

					currentOrder.products.forEach((product)=>{
						return Product.findById(product.productId).then(foundProduct =>{
							if(foundProduct == null){
								return false;
							}
							else{
								foundProduct.orders.push({orderId:currentOrder._id})
								foundProduct.save()
							}
						})
					})
					return true;
				}
			})
		}
	})
}
//REAL CODE


//Tested codes for checkout but unfortunately didd not work :'(
	// const userOrder = await User.findById(tokenId.id).then(user =>{
	// 	if(user == null){
	// 		return false
	// 	}
	// 	else{
	// 		user.orders.push({
	// 			products:cart.products,
	// 			totalAmount:cart.totalAmount
	// 		})

	// 		return user.save().then((updatedUser, error)=>{
	// 			if(error){
	// 				return false;
	// 			}
	// 			else{
	// 				const currentOrder = updatedUser.orders[updatedUser.orders.length-1]
	// 				let newData = {
	// 					currentOrder:currentOrder._id,
	// 					status:true
	// 				}
	// 				return newData;
	// 			}
	// 		})
	// 	}
	// })

	// const productOrder = await cart.products.map((product)=>{
	// 	return new Promise((resolve)=>{
	// 		return Product.findById(product.productId).then(foundProduct =>{
	// 			if(foundProduct == null){
	// 				return false;
	// 			}
	// 			else{
	// 				foundProduct.orders.push({orderId:userOrder.currentOrder})
	// 				foundProduct.save()
	// 				resolve();
	// 			}
	// 		})
	// 	})
	// })
	// console.log(productOrder)
	// Promise.all(productOrder).then((result)=>console.log(result))
	// return userOrder
	// console.log(productOrder)
	// // if(userOrder && productOrder){
	// 	return true;
	// }
	// else{
	// 	return false;
	// }


//nicee try but this won't work because of the nature of the for loop here. So maybe it is using forEach. 

	// cart.products.forEach((items)=>{
	// 	return Product.findById(items.productId).then((result, error)=>{
	// 		if(result === null){
	// 			return false
	// 		}
	// 		else{
	// 			return true
	// 		}
	// 	})
	// })


/*
require the other files such as the routes and models here.

.register

.login

.updateAccess

//optional

getAllusers
getUser


*/