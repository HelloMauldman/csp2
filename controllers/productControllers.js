const Product = require("../models/product")
const User = require("../models/user")


module.exports.getAllProducts = () =>{
	return Product.find().then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result;
		}
	})
}

module.exports.getAllActiveProduct = () =>{
	return Product.find({isActive: true}).then((result, error)=>{
		if(error){
			return false;
		}
		else{
			return result
		}
	})
}

module.exports.createProduct = (data)=>{
	let {userId, userAdmin, reqBody} = data;
	return User.findById({_id:userId}).then((result, error)=>{
		if(error){
			return "false on findOne";
		}
		else{
			if(result.isAdmin == true){
				let newProduct = new Product({
					name: reqBody.name,
					description: reqBody.description,
					price: reqBody.price,
				})

				return newProduct.save().then((result, error)=>{
					if(error){
						return "false on newProd.save";
					}
					else{
						return result;
					}
				})
			}
			else{
				return "User is not an Admin."
			}
		}
	})
}

module.exports.getProduct = (productId)=>{
	return Product.findById(productId).then((result, error)=>{
		if(error){
			return false;
		}
		else if(result == null){
			return "product not found."
		}
		else{
			return result
		}
	})
}

module.exports.updateProduct = (data) =>{
	let {tokenId, productId, reqBody} = data
	return User.findById(tokenId.id).then((result, error)=>{
		if(result.isAdmin == true){
			return Product.findByIdAndUpdate(productId, reqBody,{returnDocument:"after"}).then((result, error)=>{
				if(error){
					return false;
				}
				else if(result == null){
					return "Product Not found."
				}
				else{
					return result;
				}
			})
		}
		else{
			return "User is not an admin."
		}
	})
}

module.exports.archiveProduct = (data)=>{
	let {tokenId, productId} = data;
	return User.findById(tokenId.id).then((result, error)=>{
		if(result.isAdmin == true){
			return Product.findByIdAndUpdate(productId, {isActive:false}, {returnDocument:"after"}).then((result, error)=>{
				if(error){
					return false;
				}
				else if(result == null){
					return "Product Not found."
				}
				else{
					return result;
				}
			})
		}
		else{
			return "User is not an admin."
		}
	})
}

module.exports.activateProduct = (data)=>{
	let {tokenId, productId} = data;
	return User.findById(tokenId.id).then((result, error)=>{
		if(result.isAdmin == true){
			return Product.findByIdAndUpdate(productId, {isActive:true}, {returnDocument:"after"}).then((result, error)=>{
				if(error){
					return false;
				}
				else if(result == null){
					return "Product Not found."
				}
				else{
					return result;
				}
			})
		}
		else{
			return "User is not Admin."
		}
	})
}
/*
require the other files such as the routes and models here.

.getAllProduct

.getAllActiveProduct

.createProuct

.updateProduct

.archiveProduct


*/