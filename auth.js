const jwt = require("jsonwebtoken");
const secret = "CapstoneProjEcommerceAPI"


module.exports.createAccessToken = (user)=>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization
	// console.log(token)
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "failed"})
			}
			else{
				next()
			}
		})
	}
	else{
		return res.send("it is undefined.")
	}
}

module.exports.decode = (token) =>{
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data)=>{
			if(err){
				return null
			}
			else{
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}
}