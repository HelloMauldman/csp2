const mongoose = require("mongoose");

let productSchema = new mongoose.Schema({
	name:{
		type:String,
		require:[true, "Name is required."]
	},
	description:{
		type:String,
		require:[true, "Description is required."]
	},
	price:{
		type:Number,
		require:[true, "Price is required."]
	},
	isActive:{
		type:Boolean,
		default:true
	},
	createdOn:{
		type:Date,
		default:new Date()
	},
	orders : [ 
		{
			orderId : {
				type : String,
				required: [true, "Order ID is required"]
			}
		}
	]

})

module.exports = mongoose.model("Products", productSchema)