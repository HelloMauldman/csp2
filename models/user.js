const mongoose = require("mongoose");

let userSchema = new mongoose.Schema({
	email:{
		type:String,
		require:[true, "Email is required."]
	},
	password:{
		type:String,
		require:[true, "Password is requried."]
	},
	isAdmin:{
		type:Boolean,
		default: false
	},
	orders : [ 
		{
			products : [
				{
					productId : {
						type: String,
						required : [true, "Product Id is required"]
					},
					quantity : {
						type: Number,
						required : [true, "Product quantity is required"]
					}
				}
			],
			totalAmount : {
				type: Number,
				required : [true, "Total amount is required"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);